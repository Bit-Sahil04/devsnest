import os
import re


header = \
"""
<link rel="stylesheet" href="style.css">
<body>

<div class="header">
    <h2> Devnest Take-Home Assignments </h2>
    <h3> @Bit-Sahil04 </h3>
</div>
<section>
"""

card = """
<div class="card">
    {0}
    <div class="day">
        <p>{1}</p>
        
    </div>
    <div class="title">
        <p>PLACEHOLDER</p>
    </div>
    </a>
</div>
"""

footer = """
</section>
</body>
"""

components = [header]
days = []
for dir in os.listdir("."):
    if "day" in dir:
        dir = dir.split(".")[-1]
        days.append(dir)

days.sort()

for dir in days:
    html_link = f"<a href='{dir}\\index.html'>\n"
    generated_div = card.format(html_link, dir)
    components.append(generated_div)
        


components.append(footer)

with open("index.html", 'w+') as f:
	f.writelines(components)
