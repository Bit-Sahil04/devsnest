const box = document.querySelector('.box');
const question = document.querySelector('.question');
const nav = document.querySelector('.nav');
const answers = document.querySelectorAll('.container');
var currentQues = 0;
var score = 0;

const questions =
    {
        "What is 2+3 ?":[["5"], "4", "3", "9", "5"],
        "What is 5+4 ?":[["9"], "4", "3", "9", "5"],
        "What is 5-2 ?":[["3"], "4", "3", "9", "5"],
        "What is 3*3 ?":[["9", "9"], "4", "9", "9", "5"],
        "What is 1+3 ?":[["4"], "4", "3", "9", "5"],

    };


function submit() {
    let a = Object.values(questions);
    let correct_ans = false; // flag to check if all marked answers are correct
    let checked_ans = document.querySelectorAll('.check:checked');
    
    if (checked_ans !== null && checked_ans.length == a[currentQues][0].length){
        for (let i = 0; i < checked_ans.length; i++){
            let checked_p = checked_ans[i].nextSibling.nextSibling.innerText;
            if (a[currentQues][0][i] === checked_p){
                correct_ans = true;
            }
            else{
                correct_ans = false;
                break;
            }
        }
    }
    score += correct_ans;
    currentQues += 1;
    updateQues();
}


function updateQues(){
    let q = Object.keys(questions);
    let a = Object.values(questions);
    
    if (currentQues < q.length)
        question.innerHTML = `<p> ${q[currentQues]}</p>`;
    else{ // end game condition
       
       box.innerHTML = "";
       box.classList.add('completed');
       
       let msg = document.createElement('h1');
       msg.innerText = "Thank you for your time!" 
       
       let msg2 = document.createElement('h2')
       msg2.innerText = `Your final grade is ${score} / ${currentQues} !`
       box.appendChild(msg);
       box.appendChild(msg2);
       return;
    }
    answers.forEach((option, index) => {
        console.log()
        option.innerHTML = "";
        let bx = document.createElement('input');
        bx.type = "checkbox";
        bx.classList.add("check")
        bx.onclick = selected;
        option.appendChild(bx);
        
        let spn = document.createElement('span');
        spn.classList.add("checkmark");
        option.appendChild(spn);

        let inp = document.createElement('p');
        inp.innerText = `${a[currentQues][index + 1]}`;
        option.appendChild(inp);
    })
}


function selected(){
    if (document.querySelector('.check:checked') !== null){
        nav.classList.add('nav-active');
    }
    else{
        nav.classList.remove('nav-active');
    }
}

updateQues();