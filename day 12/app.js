const tasklist = document.getElementById("myUL");
var session = window.localStorage;

function updateStorage() {
  session.clear();
  for (let i = 0; i < tasklist.children.length; i++) {
    let task = tasklist.children[i];
    
    if (
      task.classList.contains("checked") &&
      task.innerHTML.indexOf("#!True#!") == -1
    ) {
      session.setItem(`${i}`, `#!TRUE#!${task.innerHTML}`);
    } else {
      session.setItem(`${i}`, `${task.innerHTML}`);
    }
  }
}

function addTask(value) {
  let newTask = document.createElement("li");

  newTask.innerHTML = `<p>${value}</p>`;
  appendClose(newTask);
  newTask.onclick = function () {
    markComplete(newTask);
  };
  tasklist.appendChild(newTask);
}

function deleteElement(e) {
  e.remove();
  updateStorage();
}

function appendClose(parent) {
  let newspan = document.createElement("span");
  newspan.classList.add("close");
  newspan.innerText = "🗑";
  newspan.onclick = function () {
    deleteElement(parent);
  };
  parent.appendChild(newspan);
}

function markComplete(task) {
  task.classList.toggle("checked");
  updateStorage();
}

function setDefaults() {
  let defaultTasks = ["I", "love", "to", "make", "HTML & CSS", "pages"];
  for (let i = 0; i < defaultTasks.length; i++) {
    addTask(defaultTasks[i]);
  }
  markComplete(tasklist.children[1]);
}

function newElement() {
  let task = document.getElementById("myInput");
  if (task.value.trim() !== "" || task.value.length >= 250) {
    addTask(task.value);
  } else {
    alert("Invalid Task!");
  }
  task.value = "";
  updateStorage();
}

function loadFromStorage() {
  for (let i = 0; i < session.length; i++) {
    let task = session.getItem(`${i}`);
    let task_slice = task.slice(0, 8);
    if (task_slice !== "#!TRUE#!") addTask(task);
    else {
      addTask(task.slice(8, task.length));
      tasklist.children[i].classList.toggle("checked");
    }
  }
}

if (session.length == 0) setDefaults();
else loadFromStorage();
